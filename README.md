# Grader Extension Frontend

### Build
config the parameters in `.env` and then
```bash
npm install
npm run build
```
The result is the file `dist/grader-extension-frontend.js`

### Usage
After building, you can then just
```html
<script src="/path/to/script/grader-extension-frontend.js"></script>
```
at the end of body.

### Reference
---
##### default GENERAL_PREFIX=GraderExtension
---

##### async *{ GENERAL_PREFIX }*.genKeyPairAndSend(id: string)
- generate Ed25519's keypair using `tweetnacl.js` and save to
  `window.localStorage.{ GENERAL_PREFIX }_id_{id}`.
  Then, send POST req to `{ BACKEND_HOST }/create` with the JSON
  ```js
  {
      id: { id },
      publicKey: { base64encode(publicKey: bytearray) }
  }
  ```

###### Possible Error
- `{ msg: '"id" should be string.' }`
- Expected Axios error (expected in some scenario)
- Another Unexpected error (such as network error, etc.)

---

##### async *{ GENERAL_PREFIX }*.submitCode(id: string, code: string)
- Get latest public and private key of *{ id }* from localStorage. Then, sign code using `tweetnacl.js` and then send POST req to `{ BACKEND_HOST }/submit` with the JSON
```js
{
    id: { id },
    publicKey: { base64encode(publicKey: bytearray) },
    signedCode: { base64encode(signedCode: bytearray) }
}
```

###### Possible Error
- ``{ msg: `Keypair not exists for id: ${id}` }``
- `{ msg: '"id" should be string.' }`
- `{ msg: '"code" should be string' }`
- Expected Axios error (expected in some scenario)
- Another Unexpected error (such as network error, etc.)

---

### Author
- Punyawat Rujipiranunt
