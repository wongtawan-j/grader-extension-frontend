import { generalPrefix } from './config'

export function local_set(k, v) {
    return window.localStorage.setItem(`${generalPrefix}_${k}`, v)
}

export function local_get(k) {
    return window.localStorage.getItem(`${generalPrefix}_${k}`)
}

export function local_remove(k) {
    return window.localStorage.removeItem(`${generalPrefix}_${k}`)
}
