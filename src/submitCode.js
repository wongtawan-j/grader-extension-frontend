import { checkKeypairExistForId, getKeypairForId } from './utils'
import { bytesToBase64, base64ToBytes } from './base64'
import Axios from 'axios'
import { backendHost } from './config'
import { sign } from 'tweetnacl'

export async function submitCode(id, code) {
    if(!checkKeypairExistForId(id)) {
        throw {
            msg: `Keypair not exists for id: ${id}`
        }
    }

    if (typeof id !== 'string') {
        throw {
            msg: '"id" should be string.'
        }
    }
    
    if(typeof code !== 'string') {
        throw {
            msg: '"code" should be string'
        }
    }

    const { pri, pub } = getKeypairForId(id)
    const secretKey = base64ToBytes(pri)
    const codeByteArray = new TextEncoder("utf-8").encode(code)

    const signedCode = sign(codeByteArray, secretKey)
    const base64SignedCode = bytesToBase64(signedCode)

    try {
        const res = await Axios.post(backendHost + '/submit', { studentID: parseInt(id), publicKey: pub, signedCode: base64SignedCode })
        console.log(JSON.stringify(res))
    } catch (error) {
        console.error(JSON.stringify(error.error))
        throw error
    }
}
