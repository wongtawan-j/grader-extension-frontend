import 'core-js/stable'
import 'regenerator-runtime/runtime'

export function hello() {
    console.log("hello, world!")
}

export { generalPrefix } from './config'

export { genKeyPairAndSend } from './genKeyPairAndSend'
export { submitCode } from './submitCode'
